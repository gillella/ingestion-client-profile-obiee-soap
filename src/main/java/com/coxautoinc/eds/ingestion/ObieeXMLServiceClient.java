package com.coxautoinc.eds.ingestion;

import com.obiee.analytics.soap.*;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;

import com.coxautoinc.eds.common.DefaultValue;
import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;
import org.apache.deltaspike.cdise.api.ContextControl;
import org.apache.deltaspike.core.api.provider.BeanProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


/**
 * Created by akgillella on 7/20/16.
 */
//@Singleton
public class ObieeXMLServiceClient {

    private static final Logger logger = LoggerFactory.getLogger(ObieeXMLServiceClient.class);

    @Inject
    @DefaultValue(key="obiee.atg.user", value="user")
    private String user;

    @Inject
    @DefaultValue(key="obiee.atg.password", value="pwd")
    private String password;

    @Inject
    @DefaultValue(key="obiee.analytics.sessionService.url", value="http://localhost/analytics-ws/saw.dll?SoapImpl=nQSessionService")
    private String sessionServiceUrl;

    @Inject
    @DefaultValue(key="obiee.analytics.xmlService.url", value="http://localhost/analytics-ws/saw.dll?SoapImpl=xmlViewService")
    private String xmlServiceUrl;

    @Inject
    @DefaultValue(key="obiee.atg.result.data.file.path", value="/tmp")
    private String filePath;

    @Inject
    @DefaultValue(key="obiee.atg.result.tmp.file", value="/tmp")
    private String tmpFilePath;

    @Inject
    @DefaultValue(key="obiee.atg.result.start.year", value="2016")
    private Integer startYear;

    @Inject
    @DefaultValue(key="obiee.atg.result.start.month", value="07")
    private Integer startMonth;

    @Inject
    @DefaultValue(key="obiee.atg.result.end.year", value="2016")
    private Integer endYear;

    @Inject
    @DefaultValue(key="obiee.atg.result.end.month", value="09")
    private Integer endMonth;


    SAWSessionServiceLocator awsessionservicelocator;

    XmlViewServiceLocator xmlViewServiceLocator;

    private SAWSessionServiceSoap_PortType session;

    private XmlViewServiceSoap_PortType xmlService;



    @PostConstruct
    public void init(){

        try {

            awsessionservicelocator = new SAWSessionServiceLocator();

            xmlViewServiceLocator = new XmlViewServiceLocator();

            session = awsessionservicelocator.getSAWSessionServiceSoap(new URL(sessionServiceUrl));

            xmlService = xmlViewServiceLocator.getXmlViewServiceSoap(new URL(xmlServiceUrl));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private String logOnSessionService(){

        String m_sessionID = null;

        try{

            m_sessionID = session.logon(user, password);

        } catch(Exception e){
            e.printStackTrace();
        }
        System.out.println("Session id : "+m_sessionID);
        return m_sessionID;
    }


    private void logOffSessionService(String sessionId){

        try{

            session.logoff(sessionId);

        } catch(Exception e){
            e.printStackTrace();
        }
    }


    public void executeQuery(String queryName, String query, String sessionId){

        String xmlResult = null;

        String queryId = null;

        StringBuffer fullResult = new StringBuffer();

        XMLQueryExecutionOptions queryOptions = new XMLQueryExecutionOptions();
        queryOptions.setMaxRowsPerPage(10000);
        queryOptions.setAsync(true);

        try{

            QueryResults results = xmlService.executeSQLQuery(query,
                    XMLQueryOutputFormat.fromString("SAWRowsetData"), queryOptions, sessionId);


            logger.info("Sleeping a minute to give query some time");

            Thread.sleep(60000);
            queryId = results.getQueryID();

            logger.info("QueryId: {}", queryId);
            logger.info("now lets check the results");

            String tmpXMLFileName = tmpFilePath+File.separator+queryName+".xml";
            File tmpFile = new File(tmpXMLFileName);

            if(tmpFile.exists()){
                logger.info("Found tmp file before execute query...deleting it)");
                tmpFile.delete();
            }

            FileWriter fw = new FileWriter(tmpXMLFileName,true);
            fw.write("<rowsets>\n");
            fw.flush();

            //fullResult.append("<rowsets>");
            while(!results.isFinished()){

                logger.info("Getting Next results ");
                results = xmlService.fetchNext(queryId, sessionId);
                logger.info("RowSet byte size: {}", results.getRowset().length());

                if(results.getRowset().length() == 0){

                    logger.info("Sleeping a minute to give query some more time");
                    Thread.sleep(60000);
                }

                xmlResult = results.getRowset();
                fw.write(xmlResult);
                fw.flush();
            }
            fw.write("</rowsets>\n");
            fw.flush();
            fw.close();

        } catch(Exception e){
            e.printStackTrace();
        }

    }


    public void parseResult(String queryName, String reportHeader) throws Exception{

        String tmpXMLFileName = tmpFilePath+File.separator+queryName+".xml";
        File tmpFile = new File(tmpXMLFileName);

        if(tmpFile.exists()){
            logger.info("Found tmp file with xml result)");
        } else {
            logger.info("tmp file with xml result does not exist....exiting)");
            System.exit(1);
        }


        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        ObieeQueryResultParser xmlResultHandler = new ObieeQueryResultParser();
        xmlResultHandler.setOutputFileName(filePath+File.separator+ queryName+".csv");
        xmlResultHandler.setReportHeader(reportHeader);
        saxParser.parse(tmpFile, xmlResultHandler);


    }


    public void parseResultWithDOM(String queryName, String reportHeader) throws Exception{

        String tmpXMLFileName = tmpFilePath+File.separator+queryName+".xml";
        File tmpFile = new File(tmpXMLFileName);

        if(tmpFile.exists()){
            logger.info("Found tmp file with xml result)");
        } else {
            logger.info("tmp file with xml result does not exist....exiting)");
            System.exit(1);
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();





        Document document = builder.parse(tmpFile);

        NodeList rowsets = document.getDocumentElement().getChildNodes();

        FileWriter fw = new FileWriter(filePath+File.separator+ queryName+".csv",true);
        fw.write(reportHeader+"\n");

        for (int x = 0; x < rowsets.getLength(); x++) {

            Node rowset = rowsets.item(x);

            NodeList rows = rowset.getChildNodes();

            for (int i = 0; i < rows.getLength(); i++) {

                Node row = rows.item(i);

                NodeList columns = row.getChildNodes();

                for(int j = 0; j < columns.getLength(); j++){

                    Node column = columns.item(j);

                    if (column.getNodeType() == Node.ELEMENT_NODE) {

                        Element elem = (Element) column;
                        fw.write(elem.getTextContent()+",");

                    }

                }
                fw.write("\n");
                fw.flush();
            }

        }
        fw.close();
    }




    public static void main(String[] args) throws Exception {

        String reportName = null;
        if(args.length > 0){
            reportName = args[0];
        } else {
            logger.info("Please supply a report name to run as program argument.");

            logger.info("Supported reports generated by this program :");

            logger.info("PULL_QUARTERLY_REVENUE_ALL");
            logger.info("PRODUCT_MONTHLY_EXTRACT");
            logger.info("MARKETING_RMB_MONTHLY_EXTRACT");
            logger.info("MANHEIM_QUARTERLY_EXTRACT_BUY_SELL");
            logger.info("FRANCHISE_NAMEPLATES_EXTRACT");
            logger.info("AR_AGING_REPORT");
            System.exit(1);
        }

        //CDI container initialization
        CdiContainer cdiContainer = CdiContainerLoader.getCdiContainer();
        cdiContainer.boot();
        ContextControl contextControl = cdiContainer.getContextControl();
        contextControl.startContexts();

        final ObieeXMLServiceClient client = BeanProvider.getContextualReference(ObieeXMLServiceClient.class);

        String sessionId = client.logOnSessionService();
        String xmlResult = null;


        switch(reportName){
            case "PULL_QUARTERLY_REVENUE_ALL":
            {
                String modifiedQuery = ClientProfileQueries.PULL_QUARTERLY_REVENUE_ALL
                        .replaceAll("@startYear", String.format("%4d", client.startYear))
                        .replaceAll("@startMonth", String.format("%02d", client.startMonth));
                client.executeQuery(ClientProfileQueries.QueryName_PULL_QUARTERLY_REVENUE_ALL, modifiedQuery, sessionId);
                client.parseResult(ClientProfileQueries.QueryName_PULL_QUARTERLY_REVENUE_ALL, ClientProfileQueries.HEADER_PULL_QUARTERLY_REVENUE_ALL);
                break;
            }
            case "PRODUCT_MONTHLY_EXTRACT":
            {
                client.executeQuery(ClientProfileQueries.QueryName_PRODUCT_MONTHLY_EXTRACT, ClientProfileQueries.PRODUCT_MONTHLY_EXTRACT, sessionId);
                client.parseResult(ClientProfileQueries.QueryName_PRODUCT_MONTHLY_EXTRACT, ClientProfileQueries.HEADER_PRODUCT_MONTHLY_EXTRACT);
                break;
            }
            case "MARKETING_RMB_MONTHLY_EXTRACT":
            {
                client.executeQuery(ClientProfileQueries.QueryName_MARKETING_RMB_MONTHLY_EXTRACT, ClientProfileQueries.MARKETING_RMB_MONTHLY_EXTRACT, sessionId);
                client.parseResult(ClientProfileQueries.QueryName_MARKETING_RMB_MONTHLY_EXTRACT, ClientProfileQueries.HEADER_MARKETING_RMB_MONTHLY_EXTRACT);
                break;
            }
            case "MANHEIM_QUARTERLY_EXTRACT_BUY_SELL":
            {
                String modifiedQuery = ClientProfileQueries.MANHEIM_QUARTERLY_EXTRACT_BUY_SELL
                        .replaceAll("@startYear", String.format("%4d", client.startYear))
                        .replaceAll("@startMonth", String.format("%02d", client.startMonth))
                        .replaceAll("@endYear", String.format("%4d", client.endYear))
                        .replaceAll("@endMonth", String.format("%02d", client.endMonth));
                System.out.println(modifiedQuery);
                client.executeQuery(ClientProfileQueries.QueryName_MANHEIM_QUARTERLY_EXTRACT_BUY_SELL, modifiedQuery, sessionId);
                client.parseResult(ClientProfileQueries.QueryName_MANHEIM_QUARTERLY_EXTRACT_BUY_SELL, ClientProfileQueries.HEADER_MANHEIM_QUARTERLY_EXTRACT_BUY_SELL);
                break;
            }
            case "FRANCHISE_NAMEPLATES_EXTRACT":
            {
                client.executeQuery(ClientProfileQueries.QueryName_FRANCHISE_NAMEPLATES_EXTRACT, ClientProfileQueries.FRANCHISE_NAMEPLATES_EXTRACT, sessionId);
                client.parseResult(ClientProfileQueries.QueryName_FRANCHISE_NAMEPLATES_EXTRACT, ClientProfileQueries.HEAEDR_FRANCHISE_NAMEPLATES_EXTRACT);
                break;

            }
            case "AR_AGING_REPORT":
            {
                client.executeQuery(ClientProfileQueries.QueryName_AR_AGING_REPORT, ClientProfileQueries.AR_AGING_REPORT, sessionId);
                client.parseResult(ClientProfileQueries.QueryName_AR_AGING_REPORT, ClientProfileQueries.HEADER_AR_AGING_REPORT);
                break;
            }
            default:
                throw new IllegalArgumentException("Invalid report name: " + reportName);
        }

        client.logOffSessionService(sessionId);

    }
}
